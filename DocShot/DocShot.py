import keyboard
import pyautogui
from docx.shared import Inches
import docx
import os
import ctypes
import tkinter as tk
from tkinter import simpledialog

shotfile = "shot.png"   
capture = 'ctrl+q'
close = 'ctrl+b'
paragraph = 'ctrl+y'
doc = docx.Document()
ctypes.windll.user32.MessageBoxW(0, 'Welcome to DocShot!!!', 'Info', 0x1000)

def do_cap():
    try:

        shot = pyautogui.screenshot() # take screenshot 
        shot.save(shotfile) # save screenshot
        
        doc.add_picture(shotfile, width=Inches(7))
        doc.save('Output.docx')  # update document
        os.remove(shotfile)
        ctypes.windll.user32.MessageBoxW(0, 'Screenshot saved successfully', 'Confirmation', 0x1000)
    except Exception as e:
        print("Capture Error:", e)

keyboard.add_hotkey(capture, do_cap) 

print("Started. Waiting for", capture)
 
def do_close():
    try:
        ctypes.windll.user32.MessageBoxW(0, 'Document saved successfully', 'Confirmation', 0x1000)
        os.system("TASKKILL /F /IM DocShot.exe")
    except Exception as e:
        print("Error:", e)

keyboard.add_hotkey(close, do_close)

def do_paragraph():
    try:
        ROOT = tk.Tk()
        ROOT.withdraw()
        # the input dialog
        USER_INP = simpledialog.askstring(title="Info",
                                  prompt="Enter contents:                            ")

        doc.add_paragraph(USER_INP)
        doc.save('Output.docx')
    except Exception as e:
        print("Error:", e)

keyboard.add_hotkey(paragraph, do_paragraph)
keyboard.wait()
